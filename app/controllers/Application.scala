package controllers

import play.api._
import play.api.mvc._

import models._

object Application extends Controller {
  
  def index = Action {
    Users.create("userA", "123456") //name:String, pass:String, isAdmin:Boolean = false
    Users.create("userB", "123456") //name:String, pass:String, isAdmin:Boolean = false
    Ok(views.html.index("Your new application is ready."))
  }
  
}
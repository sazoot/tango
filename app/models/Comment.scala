 package models

import anorm._
import anorm.SqlParser._

import java.util.Date

case class Comment(
id: Id[java.lang.Long],
date: Date,
text: String,
author: Long,
post: Long
)
package models

import anorm._
import anorm.SqlParser._

import java.util.Date

case class Post(
id: Id[java.lang.Long],
date: Date,
title: String,
text: String,
photo: Option[String],
author: Long
)


package models

import anorm._
import anorm.SqlParser._
import play.api._
import play.api.db._

import play.api.Play.current

import java.util.Date

case class User(
id: Pk[Long],
name: String,
pass: String,
isAdmin: Boolean
)

object Users {
  private val userConversion = {
    get[Pk[Long]]("id") ~ get[String]("name") ~ get[String]("pass") ~ bool("isAdmin") map {
      case id~name~pass~isAdmin => User(id, name, pass, isAdmin)
    }
  }

  def create(name:String, pass:String, isAdmin:Boolean = false) {
    DB.withConnection { implicit c =>
      SQL("insert into USER (name, pass, isAdmin) values ({name}, {pass}, {isAdmin})").on(
        'name -> name,
        'pass -> pass,
        'isAdmin -> isAdmin
      ).executeInsert()
    }
  }

  def delete(id: Long) {
    DB.withConnection { implicit c =>
      SQL("delete from USER where id = {id}").on(
        'id -> id
      ).executeUpdate()
    }
  }

  def all():List[User] = {
    DB.withConnection { implicit conn =>
      SQL("select * from USER").as(Users.userConversion *)
    }
  }

  def apply(id:Long):Option[User] = {
    DB.withConnection { implicit conn =>
      SQL("select * from USER where id = {id}").on("id" -> id).as(Users.userConversion.singleOpt)
    }
  }

}
# Post schema
 
# --- !Ups

Create table Post (
	id bigint(25) NOT NULL AUTO_INCREMENT,
	date date NOT NULL,
	title varchar(140) NOT NULL,
	text text NOT NULL,
	photo varchar(42),
	author bigint(25),
	FOREIGN KEY (author) REFERENCES User(id), 
	PRIMARY KEY (id)
);
# Comment schema
 
# --- !Ups

Create table Comment(
	id bigint(25) NOT NULL AUTO_INCREMENT,
	date date NOT NULL,
	text text NOT NULL,
	author bigint(25) NOT NULL,
	post bigint(25) NOT NULL,
	FOREIGN KEY (author) REFERENCES User(id),
	FOREIGN KEY (post) REFERENCES Post(id),
	PRIMARY KEY (id)
)
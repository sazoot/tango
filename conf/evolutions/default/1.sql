# User schema
 
# --- !Ups

Create table User(
	id bigint(25) NOT NULL AUTO_INCREMENT,
	name varchar(25) NOT NULL,
	pass varchar(25) NOT NULL,
	isAdmin Boolean NOT NULL,
	PRIMARY KEY (id)
);